package com.summer.framework.mybatis.interceptor;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * 数据范围
 *
 * @author jiangwei

 */
@Data
@AllArgsConstructor
public class DataScope {
    private String sqlFilter;

}

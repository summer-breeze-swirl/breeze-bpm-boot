package com.summer.framework.mybatis.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 基础Dao
 *
 * @author jiangwei

 */
public interface BaseDao<T> extends BaseMapper<T> {

}

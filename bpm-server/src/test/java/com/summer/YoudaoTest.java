package com.summer;

import cn.hutool.core.io.IoUtil;
import com.summer.api.module.translate.YoudaoApi;
import com.summer.storage.service.StorageService;
import com.summer.translate.service.YoudaoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import static dm.jdbc.desc.Configuration.url;

@SpringBootTest
public class YoudaoTest {
    @Autowired
    private YoudaoService youdaoService;

    @Test
    public void translateTextTest() throws Exception {
        List<String> list = new ArrayList<>();
        list.add("商品");
        list.add("税率");
        list.add("含税单价");


        List<String> strings = youdaoService.translateText(list, "zh-CHS");
        System.out.println("========================================翻译结果为："+strings);
    }

    @Test
    public void translateTextTest2() {


        String summary = youdaoService.translateTextTwo("青梅绿茶", "zh-CHS");

        System.out.println("=============测试结果："+summary);
    }


}

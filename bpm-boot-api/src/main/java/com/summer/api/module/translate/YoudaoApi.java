package com.summer.api.module.translate;

import java.util.List;
import java.util.Map;

/**
 * 有道翻译服务API
 *
 * @author jiangwei

 */
public interface YoudaoApi {

    /**
     * 中英文互转
     * @param fromText
     * @return
     */
    String translateText(String fromText , String fromType);


}

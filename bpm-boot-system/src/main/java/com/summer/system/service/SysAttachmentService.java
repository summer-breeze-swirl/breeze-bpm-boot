package com.summer.system.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.system.entity.SysAttachmentEntity;
import com.summer.system.query.SysAttachmentQuery;
import com.summer.system.vo.SysAttachmentVO;

import java.util.List;

/**
 * 附件管理
 *
 * @author jiangwei

 */
public interface SysAttachmentService extends BaseService<SysAttachmentEntity> {

    PageResult<SysAttachmentVO> page(SysAttachmentQuery query);

    void save(SysAttachmentVO vo);

    void update(SysAttachmentVO vo);

    void delete(List<Long> idList);
}

package com.summer.system.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.system.entity.SysLogOperateEntity;
import com.summer.system.query.SysLogOperateQuery;
import com.summer.system.vo.SysLogOperateVO;

/**
 * 操作日志
 *
 * @author jiangwei

 */
public interface SysLogOperateService extends BaseService<SysLogOperateEntity> {

    PageResult<SysLogOperateVO> page(SysLogOperateQuery query);
}

package com.summer.system.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.system.entity.SysPostEntity;
import com.summer.system.query.SysPostQuery;
import com.summer.system.vo.SysPostVO;

import java.util.List;

/**
 * 岗位管理
 *
 * @author jiangwei

 */
public interface SysPostService extends BaseService<SysPostEntity> {

    PageResult<SysPostVO> page(SysPostQuery query);

    List<SysPostVO> getList();

    void save(SysPostVO vo);

    void update(SysPostVO vo);

    void delete(List<Long> idList);
}

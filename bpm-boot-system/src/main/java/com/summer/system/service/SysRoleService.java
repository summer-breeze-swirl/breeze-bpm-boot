package com.summer.system.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.system.entity.SysRoleEntity;
import com.summer.system.query.SysRoleQuery;
import com.summer.system.vo.SysRoleDataScopeVO;
import com.summer.system.vo.SysRoleVO;

import java.util.List;

/**
 * 角色
 *
 * @author jiangwei

 */
public interface SysRoleService extends BaseService<SysRoleEntity> {

	PageResult<SysRoleVO> page(SysRoleQuery query);

	List<SysRoleVO> getList(SysRoleQuery query);

	void save(SysRoleVO vo);

	void update(SysRoleVO vo);

	void dataScope(SysRoleDataScopeVO vo);

	void delete(List<Long> idList);
}

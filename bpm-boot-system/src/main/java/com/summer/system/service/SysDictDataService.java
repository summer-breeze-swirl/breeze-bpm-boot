package com.summer.system.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.system.entity.SysDictDataEntity;
import com.summer.system.query.SysDictDataQuery;
import com.summer.system.vo.SysDictDataVO;

import java.util.List;

/**
 * 数据字典
 *
 * @author jiangwei

 */
public interface SysDictDataService extends BaseService<SysDictDataEntity> {

    PageResult<SysDictDataVO> page(SysDictDataQuery query);

    void save(SysDictDataVO vo);

    void update(SysDictDataVO vo);

    void delete(List<Long> idList);

}

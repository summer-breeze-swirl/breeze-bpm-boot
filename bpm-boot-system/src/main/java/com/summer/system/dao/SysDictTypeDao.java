package com.summer.system.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.system.entity.SysDictTypeEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 字典类型
 *
 * @author jiangwei

 */
@Mapper
public interface SysDictTypeDao extends BaseDao<SysDictTypeEntity> {

}

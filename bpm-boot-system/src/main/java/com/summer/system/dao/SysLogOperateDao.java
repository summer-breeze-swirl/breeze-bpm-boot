package com.summer.system.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.system.entity.SysLogOperateEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 操作日志
 *
 * @author jiangwei

 */
@Mapper
public interface SysLogOperateDao extends BaseDao<SysLogOperateEntity> {

}

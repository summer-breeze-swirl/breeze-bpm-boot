package com.summer.system.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.system.entity.SysAttachmentEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 附件管理
 *
 * @author jiangwei

 */
@Mapper
public interface SysAttachmentDao extends BaseDao<SysAttachmentEntity> {

}

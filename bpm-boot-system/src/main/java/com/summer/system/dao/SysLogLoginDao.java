package com.summer.system.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.system.entity.SysLogLoginEntity;
import org.apache.ibatis.annotations.Mapper;

/**
 * 登录日志
 *
 * @author jiangwei

 */
@Mapper
public interface SysLogLoginDao extends BaseDao<SysLogLoginEntity> {

}

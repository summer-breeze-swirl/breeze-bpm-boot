package com.summer.system.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.system.entity.SysPostEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 岗位管理
*
* @author jiangwei
*/
@Mapper
public interface SysPostDao extends BaseDao<SysPostEntity> {

}

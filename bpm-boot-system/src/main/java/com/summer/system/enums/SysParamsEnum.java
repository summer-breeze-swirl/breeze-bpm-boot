package com.summer.system.enums;

/**
 * 系统参数 枚举
 *
 * @author jiangwei

 */
public enum SysParamsEnum {
    /**
     * 登录验证码
     */
    LOGIN_CAPTCHA
}

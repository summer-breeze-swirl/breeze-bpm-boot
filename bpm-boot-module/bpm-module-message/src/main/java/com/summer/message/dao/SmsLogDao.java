package com.summer.message.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.message.entity.SmsLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 短信日志
*
* @author jiangwei
*/
@Mapper
public interface SmsLogDao extends BaseDao<SmsLogEntity> {

}

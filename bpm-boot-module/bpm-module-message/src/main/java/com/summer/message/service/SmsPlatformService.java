package com.summer.message.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.message.entity.SmsPlatformEntity;
import com.summer.message.query.SmsPlatformQuery;
import com.summer.message.sms.config.SmsConfig;
import com.summer.message.vo.SmsPlatformVO;

import java.util.List;

/**
 * 短信平台
 *
 * @author jiangwei

 */
public interface SmsPlatformService extends BaseService<SmsPlatformEntity> {

    PageResult<SmsPlatformVO> page(SmsPlatformQuery query);

    /**
     * 启用的短信平台列表
     */
    List<SmsConfig> listByEnable();

    void save(SmsPlatformVO vo);

    void update(SmsPlatformVO vo);

    void delete(List<Long> idList);

}

package com.summer.message.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.message.entity.SmsPlatformEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 短信平台
*
* @author jiangwei
*/
@Mapper
public interface SmsPlatformDao extends BaseDao<SmsPlatformEntity> {

}

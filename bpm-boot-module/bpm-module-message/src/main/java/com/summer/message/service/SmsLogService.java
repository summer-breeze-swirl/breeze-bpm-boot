package com.summer.message.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.message.entity.SmsLogEntity;
import com.summer.message.query.SmsLogQuery;
import com.summer.message.vo.SmsLogVO;

/**
 * 短信日志
 *
 * @author jiangwei

 */
public interface SmsLogService extends BaseService<SmsLogEntity> {

    PageResult<SmsLogVO> page(SmsLogQuery query);

}

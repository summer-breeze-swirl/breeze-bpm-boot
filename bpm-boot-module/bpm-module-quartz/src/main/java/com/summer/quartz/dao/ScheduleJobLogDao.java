package com.summer.quartz.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.quartz.entity.ScheduleJobLogEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 定时任务日志
*
* @author jiangwei
*/
@Mapper
public interface ScheduleJobLogDao extends BaseDao<ScheduleJobLogEntity> {
	
}

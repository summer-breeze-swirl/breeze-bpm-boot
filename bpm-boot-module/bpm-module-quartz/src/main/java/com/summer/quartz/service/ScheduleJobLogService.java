package com.summer.quartz.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.quartz.entity.ScheduleJobLogEntity;
import com.summer.quartz.query.ScheduleJobLogQuery;
import com.summer.quartz.vo.ScheduleJobLogVO;

/**
 * 定时任务日志
 *
 * @author jiangwei

 */
public interface ScheduleJobLogService extends BaseService<ScheduleJobLogEntity> {

    PageResult<ScheduleJobLogVO> page(ScheduleJobLogQuery query);

}

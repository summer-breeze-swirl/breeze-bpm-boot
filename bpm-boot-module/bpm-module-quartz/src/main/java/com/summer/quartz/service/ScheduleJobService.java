package com.summer.quartz.service;

import com.summer.framework.common.utils.PageResult;
import com.summer.framework.mybatis.service.BaseService;
import com.summer.quartz.entity.ScheduleJobEntity;
import com.summer.quartz.query.ScheduleJobQuery;
import com.summer.quartz.vo.ScheduleJobVO;

import java.util.List;

/**
 * 定时任务
 *
 * @author jiangwei

 */
public interface ScheduleJobService extends BaseService<ScheduleJobEntity> {

    PageResult<ScheduleJobVO> page(ScheduleJobQuery query);

    void save(ScheduleJobVO vo);

    void update(ScheduleJobVO vo);

    void delete(List<Long> idList);

    void run(ScheduleJobVO vo);

    void changeStatus(ScheduleJobVO vo);
}

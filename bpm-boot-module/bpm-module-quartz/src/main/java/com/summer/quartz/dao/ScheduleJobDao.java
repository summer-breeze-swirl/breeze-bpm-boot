package com.summer.quartz.dao;

import com.summer.framework.mybatis.dao.BaseDao;
import com.summer.quartz.entity.ScheduleJobEntity;
import org.apache.ibatis.annotations.Mapper;

/**
* 定时任务
*
* @author jiangwei
*/
@Mapper
public interface ScheduleJobDao extends BaseDao<ScheduleJobEntity> {

}

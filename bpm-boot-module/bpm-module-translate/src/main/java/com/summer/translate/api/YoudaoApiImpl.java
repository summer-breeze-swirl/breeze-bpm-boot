package com.summer.translate.api;

import com.summer.api.module.translate.YoudaoApi;
import com.summer.translate.service.YoudaoService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.InputStream;
import java.util.List;

/**
 * 存储服务Api
 *
 * @author jiangwei

 */
@Component
@AllArgsConstructor
public class YoudaoApiImpl implements YoudaoApi {

    private final YoudaoService youdaoService;

    /**
     * 中英文互转
     * @param fromText
     * @param fromType
     * @return
     */
    @Override
    public String translateText(String fromText, String fromType) {

        return youdaoService.translateTextTwo(fromText , fromType);


    }
}

package com.summer.translate.service;

import java.util.List;

public interface YoudaoService {

    /**
     * 中英文相互翻译
     * @param fromTextList
     * @param fromType
     * @return
     */
    List<String> translateText(List<String> fromTextList, String fromType);

    String translateTextTwo(String fromText , String fromType);

}

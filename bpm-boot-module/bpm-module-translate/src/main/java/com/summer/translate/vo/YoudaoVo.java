package com.summer.translate.vo;

import com.fhs.core.trans.vo.TransPojo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.List;

@Data
@Schema(description = "有道翻译")
public class YoudaoVo implements Serializable, TransPojo {

    @Serial
    private static final long serialVersionUID = 1L;

    private List<String> fromTextList;

    private String fromType;

    private List<String> toTextList;
}

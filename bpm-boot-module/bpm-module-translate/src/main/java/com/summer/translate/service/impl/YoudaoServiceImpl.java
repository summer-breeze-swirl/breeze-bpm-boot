package com.summer.translate.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.summer.translate.service.YoudaoService;
import com.summer.translate.utils.AuthV3Util;
import com.summer.translate.utils.HttpUtil;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

@Service
public class YoudaoServiceImpl implements YoudaoService {

    @Value("${youdao.appKey}")
    private String APP_KEY;

    @Value("${youdao.appSecret}")
    private String APP_SECRET;


    /**
     * 中英文相互翻译
     * @param fromTextList
     * @return
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<String> translateText(List<String> fromTextList, String fromType) {
        try {

            if(ObjectUtil.isEmpty(fromTextList)){
                throw new RuntimeException("带翻译文本数据不能为空");
            }

            if(ObjectUtil.isEmpty(fromType)){
                throw new RuntimeException("源语言语种数据不能为空");
            }

            Map<String, String[]> paramMap = new HashMap<>();

            if(ObjectUtil.equal(fromType , "en" )){
                paramMap.put("from", new String[]{fromType});
                paramMap.put("to" , new String[]{"zh-CHS"});
            }else if(ObjectUtil.equal(fromType , "zh-CHS" )){
                paramMap.put("from", new String[]{fromType});
                paramMap.put("to" , new String[]{"en"});
            }
            paramMap.put("q" , fromTextList.toArray(new String[0]));

            // 添加鉴权相关参数
            AuthV3Util.addAuthParams(APP_KEY, APP_SECRET, paramMap);

            // 请求api服务
            byte[] result = HttpUtil.doPost("https://openapi.youdao.com/api", null, paramMap, "application/json");
            // 打印返回结果
            if (result != null) {
                System.out.println(new String(result, StandardCharsets.UTF_8));
                String summary = new String(result, StandardCharsets.UTF_8);

                return Collections.singletonList(summary);
            }
            System.exit(1);

        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("有道翻译发生异常！！！");
        }

        return null;


    }
    @Override
    public String translateTextTwo(String fromText , String fromType) {

        if(ObjectUtil.isEmpty(fromText)){
            throw new RuntimeException("带翻译文本数据不能为空");
        }

        if(ObjectUtil.isEmpty(fromType)){
            throw new RuntimeException("源语言语种数据不能为空");
        }

        Map<String, String> paramMap = new HashMap<>();

        if(ObjectUtil.equal(fromType , "en" )){
            paramMap.put("from", fromType);
            paramMap.put("to" , "zh-CHS");
        }else if(ObjectUtil.equal(fromType , "zh-CHS" )){
            paramMap.put("from", fromType);
            paramMap.put("to" , "en");
        }
        paramMap.put("q" , fromText);

        String salt = String.valueOf(System.currentTimeMillis());
        paramMap.put("signType", "v3");
        String curtime = String.valueOf(System.currentTimeMillis() / 1000);
        paramMap.put("curtime", curtime);
        String signStr = APP_KEY + truncate(fromText) + salt + curtime + APP_SECRET;
        String sign = getDigest(signStr);
        paramMap.put("appKey", APP_KEY);
        paramMap.put("salt", salt);
        paramMap.put("sign", sign);
        /* 处理结果 */
        try {
            // 获取翻译后的结果
            String summary = requestForHttp("https://openapi.youdao.com/api", paramMap);

            if(ObjectUtil.isNotEmpty(summary)){
                return this.convertJavaField(summary);
            }
            return null;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 将翻译后的文字转换为java字段
     * @param summary
     * @return
     */
    private String convertJavaField(String summary) {

        // 去除前后空格
        summary = summary.trim();

        // 将字段变为小写
        summary = summary.toLowerCase();

        StringBuilder result = new StringBuilder();

        String[] sumTemp = summary.split(" ");

        //然后通过for循环每个小字符串（单词）
        for (String s : sumTemp) {
            //找到单词中的第一个字母
            char ch = s.charAt(0);
            //将以第一个字母变成大写
            char c = Character.toUpperCase(ch);
            //截取除了首字母以外的单词
            String s1 = s.substring(1);
            //拼接字符串
            result.append(c).append(s1);
        }

        return Character.toLowerCase(result.toString().charAt(0)) + result.substring(1);




    }

    public static String requestForHttp(String url,Map<String,String> params) throws IOException {

        /* 创建HttpClient */
        CloseableHttpClient httpClient = HttpClients.createDefault();

        /* httpPost */
        HttpPost httpPost = new HttpPost(url);
        List<NameValuePair> paramsList = new ArrayList<NameValuePair>();
        for (Map.Entry<String, String> en : params.entrySet()) {
            String key = en.getKey();
            String value = en.getValue();
            paramsList.add(new BasicNameValuePair(key, value));
        }
        httpPost.setEntity(new UrlEncodedFormEntity(paramsList,"UTF-8"));
        try (CloseableHttpResponse httpResponse = httpClient.execute(httpPost)) {
            Header[] contentType = httpResponse.getHeaders("Content-Type");
            /* 直接显示结果 */
            HttpEntity httpEntity = httpResponse.getEntity();
            String json = EntityUtils.toString(httpEntity, "UTF-8");
            EntityUtils.consume(httpEntity);
            System.out.println("============测试结果：" + json);

            JSONObject jsonStr = JSONObject.parseObject(json);

            Object translationObj = jsonStr.get("translation");

            if(ObjectUtil.isNotEmpty(translationObj)){
                return translationObj.toString();
            }else{
                return null;
            }


        }
    }

    /**
     * 生成加密字段
     */
    public static String getDigest(String string) {
        if (string == null) {
            return null;
        }
        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        byte[] btInput = string.getBytes(StandardCharsets.UTF_8);
        try {
            MessageDigest mdInst = MessageDigest.getInstance("SHA-256");
            mdInst.update(btInput);
            byte[] md = mdInst.digest();
            int j = md.length;
            char str[] = new char[j * 2];
            int k = 0;
            for (byte byte0 : md) {
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            return new String(str);
        } catch (NoSuchAlgorithmException e) {
            return null;
        }
    }

    /**
     *
     * @param result 音频字节流
     * @param file 存储路径
     */
    private static void byte2File(byte[] result, String file) {
        File audioFile = new File(file);
        FileOutputStream fos = null;
        try{
            fos = new FileOutputStream(audioFile);
            fos.write(result);

        }catch (Exception e){
        }finally {
            if(fos != null){
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public static String truncate(String q) {
        if (q == null) {
            return null;
        }
        int len = q.length();
        String result;
        return len <= 20 ? q : (q.substring(0, 10) + len + q.substring(len - 10, len));
    }

}
